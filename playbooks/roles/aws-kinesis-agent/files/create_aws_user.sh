getent group aws-kinesis-agent-user >/dev/null || groupadd -g 497 aws-kinesis-agent-user
getent passwd aws-kinesis-agent-user >/dev/null || useradd -u 497 -g 497 -m -d /usr/share/aws-kinesis-agent -s /sbin/nologin -c "Streaming Data Agent" aws-kinesis-agent-user
