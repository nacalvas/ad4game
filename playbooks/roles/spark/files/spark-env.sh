#!/usr/bin/env bash

SPARK_MASTER_WEBUI_PORT=8081

SPARK_WORKER_CORES=4
SPARK_WORKER_MEMORY=4g
SPARK_WORKER_OPTS="-Dspark.worker.cleanup.enabled=true -Dspark.worker.cleanup.interval=1800 -Dspark.worker.cleanup.appDataTtl=43200"
