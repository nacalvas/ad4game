server {
    listen   82;
    listen   83;
    server_name play.xmmorpg.com;
    access_log off;
    error_log /dev/null crit;

    include common/locations-openx;
}
