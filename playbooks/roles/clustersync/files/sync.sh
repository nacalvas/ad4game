### 10.100.0.136 [delivery01.ad4game.com] sync
rsync -avz --delete /var/openx/shared/rsync/images/ clustersync@10.100.0.136:~/sync/images/
rsync -avz --delete /var/openx/shared/rsync/games/ clustersync@10.100.0.136:~/sync/games/

### 10.100.0.137 [delivery02.ad4game.com] sync
rsync -avz --delete /var/openx/shared/rsync/images/ clustersync@10.100.0.137:~/sync/images/
rsync -avz --delete /var/openx/shared/rsync/games/ clustersync@10.100.0.137:~/sync/games/

### 10.100.0.138 [delivery03.ad4game.com] sync
rsync -avz --delete /var/openx/shared/rsync/images/ clustersync@10.100.0.138:~/sync/images/
rsync -avz --delete /var/openx/shared/rsync/games/ clustersync@10.100.0.138:~/sync/games/

### 10.100.0.139 [delivery04.ad4game.com] sync
rsync -avz --delete /var/openx/shared/rsync/images/ clustersync@10.100.0.139:~/sync/images/
rsync -avz --delete /var/openx/shared/rsync/games/ clustersync@10.100.0.139:~/sync/games/

### 10.100.0.140 [delivery05.ad4game.com] sync
rsync -avz --delete /var/openx/shared/rsync/images/ clustersync@10.100.0.140:~/sync/images/
rsync -avz --delete /var/openx/shared/rsync/games/ clustersync@10.100.0.140:~/sync/games/

### 10.100.0.141 [dev01.ad4game.com] sync
rsync -avz --delete /var/openx/shared/rsync/images/ clustersync@10.100.0.141:~/sync/images/
rsync -avz --delete /var/openx/shared/rsync/games/ clustersync@10.100.0.141:~/sync/games/

### 10.100.0.142 [tracking01.ad4game.com] sync
rsync -avz --delete /var/openx/shared/rsync/images/ clustersync@10.100.0.142:~/sync/images/
rsync -avz --delete /var/openx/shared/rsync/games/ clustersync@10.100.0.142:~/sync/games/

### 10.100.0.143 [tracking02.ad4game.com] sync
rsync -avz --delete /var/openx/shared/rsync/images/ clustersync@10.100.0.143:~/sync/images/
rsync -avz --delete /var/openx/shared/rsync/games/ clustersync@10.100.0.143:~/sync/games/

### 10.100.0.133 [admin_backup.ad4game.com] sync
rsync -avz --delete /var/openx/shared/rsync/images/ clustersync@10.100.0.133:~/sync/images/
rsync -avz --delete /var/openx/shared/rsync/games/ clustersync@10.100.0.133:~/sync/games/
