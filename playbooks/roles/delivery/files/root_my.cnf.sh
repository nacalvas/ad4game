#!/bin/bash


touch /root/.my.cnf
chmod 600 /root/.my.cnf

pass=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w24 | head -1)
# Make sure that NOBODY can access the server without a password
mysql -e "UPDATE mysql.user SET Password = PASSWORD(\"$pass\") WHERE User = 'root'"
# Kill the anonymous users
mysql -e "DROP USER ''@'localhost'"
# Because our hostname varies we'll use some Bash magic here.
mysql -e "DROP USER ''@'$(hostname)'"
# Kill off the demo database
mysql -e "DROP DATABASE test"
# Make our changes take effect
mysql -e "FLUSH PRIVILEGES"

cat <<EOF>/root/.my.cnf
[client]
user=root
password=$pass
EOF

chmod 400 /root/.my.cnf
