HTTP/1.0 200 OK
Content-Type: text/html
Content-Length: 475
Set-Cookie: A4G_PROXY=ha1_default_response; path=/


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-25223609-1', 'auto');
  var dimensionValue = 'LB1';
  ga('set', 'dimension1', dimensionValue);
  ga('send', 'pageview');

</script>
