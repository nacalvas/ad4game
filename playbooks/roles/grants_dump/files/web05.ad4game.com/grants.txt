
## Grants for replicant@10.57.60.% ##
GRANT ALL PRIVILEGES ON *.* TO 'replicant'@'10.57.60.%' IDENTIFIED BY PASSWORD '5b1ceb2727d52198';

## Grants for @10.57.60.98 ##
GRANT USAGE ON *.* TO ''@'10.57.60.98';

## Grants for root@10.57.60.98 ##
GRANT ALL PRIVILEGES ON *.* TO 'root'@'10.57.60.98' WITH GRANT OPTION;

## Grants for root@127.0.0.1 ##
GRANT ALL PRIVILEGES ON *.* TO 'root'@'127.0.0.1' WITH GRANT OPTION;

## Grants for @localhost ##
GRANT USAGE ON *.* TO ''@'localhost';

## Grants for ad4gamedelivery@localhost ##
GRANT USAGE ON *.* TO 'ad4gamedelivery'@'localhost' IDENTIFIED BY PASSWORD '245b3bdf5203daee';
GRANT SELECT ON `openx`.* TO 'ad4gamedelivery'@'localhost';

## Grants for backup@localhost ##
GRANT ALL PRIVILEGES ON *.* TO 'backup'@'localhost' IDENTIFIED BY PASSWORD '260a33d6677947cb';

## Grants for nagios@localhost ##
GRANT USAGE ON *.* TO 'nagios'@'localhost' IDENTIFIED BY PASSWORD '2d19ba4f0b364985';

## Grants for openx@localhost ##
GRANT USAGE ON *.* TO 'openx'@'localhost' IDENTIFIED BY PASSWORD '6b4e7fe766ae5e32';
GRANT ALL PRIVILEGES ON `openx`.* TO 'openx'@'localhost';

## Grants for replicant@localhost ##
GRANT ALL PRIVILEGES ON *.* TO 'replicant'@'localhost' IDENTIFIED BY PASSWORD '5b1ceb2727d52198';

## Grants for root@localhost ##
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' WITH GRANT OPTION;
