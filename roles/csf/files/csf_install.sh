cd /opt
\rm -fv csf.tgz
cd /tmp
mkdir csf
cd /tmp/csf
wget https://download.configserver.com/csf.tgz
tar -xzf csf.tgz
cd csf
sh install.sh
cd /tmp
rm -rf /tmp/csf
