set showcmd " show partially entered comands on status bar
set ruler " show line, col of cursor
set rulerformat=%55(%{strftime('%a\ %b\ %e\ %I:%M\ %p')}\ %5l,%-6(%c%V%)\ %P%)
set pastetoggle=
set showmode " show me when i am in insert mode
set autoindent " keep the previous line's indentation
set nonumber " show line numbers
set incsearch " do incremental searches
set hlsearch " highlight search results
set nowrap
set nolist
set smarttab
set esckeys " allow arrow keys while inserting
set tabstop=4
set shiftwidth=4
set expandtab
set showmatch
set ignorecase " Ignore case in searches
set smartcase " Ignore ignorecase when pattern contains uppercase
set nu
set syntax=rest
syntax on
