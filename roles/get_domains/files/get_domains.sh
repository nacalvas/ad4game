#!/bin/bash

apache_vhosts=$(2>&1 httpd -S | awk '/80 namevhost/ {print $4}')
apache_aliases=$(2>&1 httpd -S | awk '/alias/ {print $2}')
apache_domains=$(echo -e "$apache_vhosts\n$apache_aliases")
nginx_domains=$(find /etc/nginx -name *.conf | xargs grep server_name | awk '!/SERVER_NAME/ && !/add_header/ {print $3}' | cut -d\; -f1)
OUTPUT=/tmp/domains.txt

write_data(){
    for domain in $nginx_domains; do
        echo "nginx,$domain"
    done
    for domain in $apache_domains; do
        echo "apache,$domain"
    done
}

write_data > $OUTPUT
