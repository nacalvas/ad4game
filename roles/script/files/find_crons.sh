#!/bin/bash

find / \( -path "/etc/cron*" -o -path "/var/spool/cron*" \) -type f
